#include "MotorController.h"

MotorController::MotorController(uint8_t pin) {
  init(pin, DEFAULT_ZERO_POINT, DEFAULT_REV_MAX, DEFAULT_FOR_MAX);
}

MotorController::MotorController(uint8_t pin, uint8_t zero, uint8_t min, uint8_t max) {
  init(pin, zero, min, max);
}

void MotorController::init(uint8_t pin, uint8_t zero, uint8_t min, uint8_t max) {
  this->pin = pin;
  this->zeroPoint = zero;
  this->fullReverse = min;
  this->fullForward = max;
  pinMode(pin, OUTPUT); 
}

void MotorController::setZero(uint8_t zero) {
  this->zeroPoint = zero;
}

void MotorController::setMinMax(uint8_t min, uint8_t max) {
  this->fullReverse = min;
  this->fullForward = max;
}


/**
 * Writes a raw pwm value to the motor controller's pin.
 */
void MotorController::writeRaw(uint8_t rawSpeed) {
  analogWrite(11, rawSpeed);
}

/**
 * taking a signed int16, set the speed of the motor controller
 * This can take direct joystick values as input.
 * This function is faster and uses less memory than setSpeedFloat(), 
 * but cannot scale its values as accurately, and ignores min and max.
 */
void MotorController::setSpeedInt16(int16_t speed) {
  //Take the given int16 speed and make it positive and centered around the zero point
  // has to be int32 to allow for negative addition of int16 to uint16
  int32_t v = speed + (((int32_t) this->zeroPoint) << 8);
  
  //0 out v so we can right-shift
  v = v < 0 ? 0 : v;
  // the output speed is the 8 most significant bits of v as uint16
  analogWrite(this->pin, v >> 8);
}

/**
 * Writes The speed of a motor attached to a controller, 
 * from full reverse (-1) to full forward (1).
 * Scales the forward and reverse ranges independently, such that:
 *   -1 outputs the controller's configured min value
 *    0 outputs the controller's configured zero value
 *    1 outputs the controller's configured max value
 * Using this function may result in less responsivness on slower microcontrollers.
 */
void MotorController::setSpeedFloat(float speed) {
  // clamp speed
  if(speed < -1) {
    speed = -1;
  } else if(speed > 1) {
    speed = 1;    
  }

  float offset;
  uint8_t out;
  if (speed < 0) {
    offset = (float)(this->fullReverse - this->zeroPoint) * speed;
    out = this->zeroPoint - (uint8_t)offset;
  } else {
    offset = (float)(this->fullForward - this->zeroPoint) * speed;
    out = this->zeroPoint + (uint8_t)offset;
  }
  analogWrite(this->pin, out);
}
