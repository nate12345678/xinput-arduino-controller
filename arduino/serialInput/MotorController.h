#pragma once

// #include "State.h"
#include "Arduino.h"

#define DEFAULT_ZERO_POINT 128
#define DEFAULT_REV_MAX 0
#define DEFAULT_FOR_MAX 255

class MotorController {
    uint8_t pin;
    uint8_t zeroPoint;
    uint8_t fullForward;
    uint8_t fullReverse;
    
    void init(uint8_t pin, uint8_t zero, uint8_t min, uint8_t max);    
  public:
    MotorController(uint8_t pin);
    MotorController(uint8_t pin, uint8_t zero, uint8_t min, uint8_t max);

    void setZero(uint8_t zero);
    void setMinMax(uint8_t min, uint8_t max);

    void writeRaw(uint8_t rawSpeed);
    void setSpeedFloat(float speed);
    void setSpeedInt16(int16_t speed);
};