#include "Robot.h"


Robot::Robot(){
  digitalWrite(LED_BUILTIN, HIGH);

  this->testMC = new MotorController(11);
}

int Robot::loop(){
  this->testMC->writeRaw(gamepad->rightTrigger);
  digitalWrite(LED_BUILTIN, LOW);
  return 0;
}

/**
 * Define your hardstop state here. 
 * This function will be called if the robot loses communication or if any robot functions return
 * any value other than 0.
 * This function will be continually run until it returns 0 again, at which point the application 
 * loop will attempt to run Robot::loop() again.
 */
int Robot::hardStop(uint32_t time) {
  digitalWrite(LED_BUILTIN, HIGH);
  this->testMC->writeRaw(0);
  if (time < TIMEOUT) {
      return 0;
  }
  return 1;
}
