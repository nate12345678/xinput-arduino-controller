#pragma once

#include "Arduino.h"
#include "SerialGamepad.h"
#include "MotorController.h"

class Robot {
  MotorController* testMC;

  public:
    Robot();
    int loop();
    int hardStop(uint32_t time);
};


