#include "SerialGamepad.h"

SerialGamepad::SerialGamepad() {
}

// Reads a serial packet and updates controller state
void SerialGamepad::updateState(uint8_t* buf) {
  this->id = (((uint16_t)buf[2]) << 8) + ((uint16_t) buf[1]);
  this->buttons = (((uint16_t)buf[4]) << 8) + ((uint16_t) buf[3]);

  // Axes
  this->thumbLX = (int)(((uint16_t)buf[6]) << 8) + ((uint16_t) buf[5]);
  this->thumbLY = (int)(((uint16_t)buf[8]) << 8) + ((uint16_t) buf[7]);
  this->thumbRX = (int)(((uint16_t)buf[10]) << 8) + ((uint16_t) buf[9]);
  this->thumbRY = (int)(((uint16_t)buf[12]) << 8) + ((uint16_t) buf[11]);
  this->leftTrigger  = buf[13];
  this->rightTrigger = buf[14];

  // Buttons
  this->dpadUp    = (this->buttons & GAMEPAD_DPAD_UP) != 0;
  this->dpadDown  = (this->buttons & GAMEPAD_DPAD_DOWN) != 0;
  this->dpadLeft  = (this->buttons & GAMEPAD_DPAD_LEFT) != 0;
  this->dpadRight = (this->buttons & GAMEPAD_DPAD_RIGHT) != 0;
  
  this->start = (this->buttons & GAMEPAD_START) != 0;
  this->back  = (this->buttons & GAMEPAD_BACK) != 0;
  this->leftStick  = (this->buttons & GAMEPAD_LEFT_THUMB) != 0;
  this->rightStick = (this->buttons & GAMEPAD_RIGHT_THUMB) != 0;
  this->leftBumper  = (this->buttons & GAMEPAD_LEFT_SHOULDER) != 0;
  this->rightBumper = (this->buttons & GAMEPAD_RIGHT_SHOULDER) != 0;
  
  this->a = (this->buttons & GAMEPAD_A) != 0;
  this->b = (this->buttons & GAMEPAD_B) != 0;
  this->x = (this->buttons & GAMEPAD_X) != 0;
  this->y = (this->buttons & GAMEPAD_Y) != 0;
}

/**
 * Converts a trigger input to a value between 0 and 1
 */
float triggerToFloat(uint8_t trigger) {
  return (float)trigger / 255.0;
}

/**
 * Converts a stick input to a value between -1 and 1
 */
float stickToFloat(int16_t stick) {
  return (float)stick / 32767.0;
}
