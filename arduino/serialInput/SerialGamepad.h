#pragma once

#include "State.h"

#define GAMEPAD_DPAD_UP 0x0001
#define GAMEPAD_DPAD_DOWN 0x0002
#define GAMEPAD_DPAD_LEFT 0x0004
#define GAMEPAD_DPAD_RIGHT 0x0008
#define GAMEPAD_START 0x0010
#define GAMEPAD_BACK 0x0020
#define GAMEPAD_LEFT_THUMB 0x0040
#define GAMEPAD_RIGHT_THUMB 0x0080
#define GAMEPAD_LEFT_SHOULDER 0x0100
#define GAMEPAD_RIGHT_SHOULDER 0x0200
#define GAMEPAD_A 0x1000
#define GAMEPAD_B 0x2000
#define GAMEPAD_X 0x4000
#define GAMEPAD_Y 0x8000

class SerialGamepad {
    uint16_t buttons;
    uint16_t id;
  public:
    int16_t thumbLX;
    int16_t thumbLY;
    int16_t thumbRX;
    int16_t thumbRY;
    uint8_t leftTrigger;
    uint8_t rightTrigger;
    
    uint8_t dpadUp;
    uint8_t dpadDown;
    uint8_t dpadLeft;
    uint8_t dpadRight;
    uint8_t start;
    uint8_t back;
    uint8_t leftStick;
    uint8_t rightStick;
    uint8_t leftBumper;
    uint8_t rightBumper;
    uint8_t a;
    uint8_t b;
    uint8_t x;
    uint8_t y;

    SerialGamepad();
    void updateState(uint8_t*);
};

float triggerToFloat(uint8_t trigger);
float stickToFloat(int16_t stick);

extern SerialGamepad* gamepad;
