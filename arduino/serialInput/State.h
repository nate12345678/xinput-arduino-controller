#pragma once

//Typedefs
#define uint16_t unsigned int
#define int16_t int
#define uint8_t unsigned char
#define uint32_t unsigned long
#define int32_t long

#define TIMEOUT 200
#define UINT_MAX 65535

#define PACKET_SIZE 15
#define MAGIC 0xAA

