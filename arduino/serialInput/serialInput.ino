#include "SerialGamepad.h"
#include "State.h"
#include "Robot.h"

uint8_t serialPacket[PACKET_SIZE];
uint8_t readBytes;
uint32_t lastPacketTimestamp;
uint8_t doHardstop = 0;

SerialGamepad* gamepad = new SerialGamepad();
Robot* robot = new Robot();

void setup() {
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB
  }
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, LOW);

  lastPacketTimestamp = millis();
}

// Main Loop function, should not be edited by user
void loop() {
  uint32_t looptime = millis();

  // Read in data, reset if we find a magic byte
  if(Serial.available()) {
    uint8_t curr = Serial.read();
    if (curr == MAGIC) {
      readBytes = 0;
    }
    serialPacket[readBytes] = curr;
    readBytes++;
  }
  // Update data if the packet is complete
  if(readBytes == PACKET_SIZE) {
    lastPacketTimestamp = looptime;
    gamepad->updateState(serialPacket);
    readBytes = 0;
  }
  
  // Do stuck detection and run the loop
  uint32_t timeSinceLast = looptime - lastPacketTimestamp;
  if(timeSinceLast >= TIMEOUT || doHardstop){
    doHardstop = robot->hardStop(timeSinceLast);
  } else if(robot->loop()) {
    doHardstop = robot->hardStop(timeSinceLast);
  }
}
