#include "ArduinoSerial.h"

ArduinoSerial::ArduinoSerial() {

}

void* ArduinoSerial::connect(char* port) {
    https://docs.microsoft.com/en-us/windows/win32/api/fileapi/nf-fileapi-createfilea
    this->serialHandle = CreateFileA(port, (GENERIC_READ | GENERIC_WRITE), 0, NULL, OPEN_EXISTING, 0x0, NULL);

    serialParams.DCBlength = sizeof(serialParams);
    if (!GetCommState(serialHandle, &serialParams)) {
        return NULL;
    }

    serialParams.DCBlength = sizeof(serialParams);
    serialParams.BaudRate = BAUD;
    serialParams.ByteSize = 8;             //ByteSize = 8
    serialParams.StopBits = ONESTOPBIT;    //StopBits = 1
    serialParams.Parity = NOPARITY;      //Parity = None

    if (!SetCommState(serialHandle, &serialParams)) {
        return NULL;
    }

    return this->serialHandle;
}

bool ArduinoSerial::write(char* buf, DWORD len, LPDWORD bytesWritten) {
    return WriteFile(this->serialHandle, buf, len, bytesWritten, NULL);
}
