#pragma once
#include <Windows.h>
#include <fileapi.h>

#define BAUD CBR_9600;
class ArduinoSerial{
        HANDLE serialHandle;
        DCB serialParams;
    public:
        ArduinoSerial();
        void* connect(char* port);        
        bool write(char* buf, DWORD len, LPDWORD bytesWritten);
        
};
