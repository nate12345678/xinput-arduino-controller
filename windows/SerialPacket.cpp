#include "./SerialPacket.h"

using namespace std;

void printPacket(DataPacket* packet) {
    cout << "id:          " << packet->id << endl;
    cout << "D-up:        " << (packet->wButtons & XINPUT_GAMEPAD_DPAD_UP) << endl;
    cout << "D-down:      " << (packet->wButtons & XINPUT_GAMEPAD_DPAD_DOWN) << endl;
    cout << "D-left:      " << (packet->wButtons & XINPUT_GAMEPAD_DPAD_LEFT) << endl;
    cout << "D-right:     " << (packet->wButtons & XINPUT_GAMEPAD_DPAD_RIGHT) << endl;
    cout << "Start:       " << (packet->wButtons & XINPUT_GAMEPAD_START) << endl;
    cout << "Back/Select: " << (packet->wButtons & XINPUT_GAMEPAD_BACK) << endl;
    cout << "L-stick:     " << (packet->wButtons & XINPUT_GAMEPAD_LEFT_THUMB) << endl;
    cout << "R-stick:     " << (packet->wButtons & XINPUT_GAMEPAD_RIGHT_THUMB) << endl;
    cout << "L-shoulder:  " << (packet->wButtons & XINPUT_GAMEPAD_LEFT_SHOULDER) << endl;
    cout << "R-shoulder:  " << (packet->wButtons & XINPUT_GAMEPAD_RIGHT_SHOULDER) << endl;
    cout << "A:           " << (packet->wButtons & XINPUT_GAMEPAD_A) << endl;
    cout << "B:           " << (packet->wButtons & XINPUT_GAMEPAD_B) << endl;
    cout << "X:           " << (packet->wButtons & XINPUT_GAMEPAD_X) << endl;
    cout << "Y:           " << (packet->wButtons & XINPUT_GAMEPAD_Y) << endl;
    cout << "LX:          " << packet->sThumbLX << endl;
    cout << "LY:          " << packet->sThumbLY << endl;
    cout << "RX:          " << packet->sThumbRX << endl;
    cout << "RY:          " << packet->sThumbRY << endl;
    cout << "L-Trigger:   " << +(packet->bLeftTrigger) << endl;
    cout << "R-Trigger:   " << +(packet->bRightTrigger) << endl;
}

// if buf < 15 bytes, expect memory overwrites
void packetToBytes(char* buf, DataPacket* packet) {
    buf[0] = MAGIC;
    *((uint16_t*)(buf + 1)) = packet->id;
    *((uint16_t*)(buf + 3)) = packet->wButtons;
    *((int16_t*)(buf + 5)) = packet->sThumbLX;
    *((int16_t*)(buf + 7)) = packet->sThumbLY;
    *((int16_t*)(buf + 9)) = packet->sThumbRX;
    *((int16_t*)(buf + 11)) = packet->sThumbRY;
    *((uint8_t*)(buf + 13)) = packet->bLeftTrigger;
    *((uint8_t*)(buf + 14)) = packet->bRightTrigger;
}