#pragma once

#include <iostream>
#include <Windows.h>
#include <Xinput.h>

#define MAGIC 0xAA

typedef struct {
    uint16_t id;
    uint16_t wButtons;
    int16_t  sThumbLX;
    int16_t  sThumbLY;
    int16_t  sThumbRX;
    int16_t  sThumbRY;
    uint8_t  bLeftTrigger;
    uint8_t  bRightTrigger;
} DataPacket;

void printPacket(DataPacket* packet);

void packetToBytes(char* buf, DataPacket* packet);