#pragma once

#ifndef UNICODE
#define UNICODE
#endif 

#include <Windows.h>

const wchar_t CLASS_NAME[]  = L"Window Class";

int WINAPI openWin(HINSTANCE hInstance, HINSTANCE hPrevInstance, PWSTR pCmdLine, int nCmdShow);
int WINAPI runWin();

LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
