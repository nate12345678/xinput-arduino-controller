#include "main.h"

// add this if not using xinput in compile path
// #pragma comment(lib, "XInput.lib")

using namespace std;

int main(HINSTANCE hInstance, HINSTANCE hPrevInstance, PWSTR pCmdLine, int nCmdShow) {
    if(openWin(hInstance, hPrevInstance, pCmdLine, nCmdShow)) return 1;
    //Find the controller to use
    DWORD dwResult;
    XINPUT_STATE state;

    // cout << "something" << endl;
    // while(1) {
    //     if(runWin()) return 0;
    // }

    while(findController(&state)){
        Sleep(CONTROLLER_FIND_DELAY);
    }

    uint16_t i = 0;
    DataPacket packetToSend;
    ArduinoSerial sp;
    char serialData[15];
    void* status = sp.connect(PORT);
    if (status == INVALID_HANDLE_VALUE || status == NULL) {
        cout << "Can't connect to device" << endl;
    }

    while (1) {
        // Simply get the state of the controller from XInput.
        dwResult = XInputGetState(controllerNum, &state);
        if(dwResult == ERROR_SUCCESS) {
            short LX = (state.Gamepad.sThumbLX > DEADBAND || state.Gamepad.sThumbLX < -DEADBAND) ? state.Gamepad.sThumbLX : 0;
            short LY = (state.Gamepad.sThumbLY > DEADBAND || state.Gamepad.sThumbLY < -DEADBAND) ? state.Gamepad.sThumbLY : 0;
            short RX = (state.Gamepad.sThumbRX > DEADBAND || state.Gamepad.sThumbRX < -DEADBAND) ? state.Gamepad.sThumbRX : 0;
            short RY = (state.Gamepad.sThumbRY > DEADBAND || state.Gamepad.sThumbRY < -DEADBAND) ? state.Gamepad.sThumbRY : 0;
            packetToSend = {
                i,
                state.Gamepad.wButtons,
                LX,
                LY,
                RX,
                RY,
                state.Gamepad.bLeftTrigger,
                state.Gamepad.bRightTrigger
            };

        } else {
            findController(&state);
            Sleep(CONTROLLER_FIND_DELAY);
            continue;
        }

        if(useSerial) {
            packetToBytes(serialData, &packetToSend);
            unsigned long bytesWritten = 0;
            if( !sp.write(serialData, 15, &bytesWritten)) {
                cout << "cant send serial to device" << endl;
            }
            printBytes(serialData, 15);
            cout << bytesWritten << endl;
        }
        Sleep(20);
        i++;
    }
    return 0;
}

void printBytes(void* buf, int len) {
    unsigned char* b = (unsigned char*)buf;
    for (int i = 0; i < len; i++) {
        cout << (unsigned short)b[i] << " ";
    }
    // cout << endl << endl;
}


int findController(XINPUT_STATE* state) {
    DWORD result;

    for (DWORD i = 0; i < XUSER_MAX_COUNT; i++ ) {
        ZeroMemory(state, sizeof(XINPUT_STATE));

        // Simply get the state of the controller from XInput.
        result = XInputGetState(i, state);

        if(result == ERROR_SUCCESS) {
            cout << "Using controller found at port:" << i << endl;
            controllerNum = i;
            return 0;
        } else if (i == XUSER_MAX_COUNT - 1) {
            // error out if no controller found
            cerr << "No controller found" << endl;
            return 1;
        }
    }
    return 0;
}