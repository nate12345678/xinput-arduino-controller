#pragma once

#include <iostream>
#include <string>
#include <Windows.h>
#include <Xinput.h>
#include <fileapi.h>

#include "./ArduinoSerial.h"
#include "./SerialPacket.h"
#include "./Window.h"

#define PORT "COM5"
#define DEADBAND 384
#define CONTROLLER_FIND_DELAY 1000

int controllerNum = 0;

const int useSerial = 1;

void printBytes(void* buf, int len);
int findController(XINPUT_STATE* state);
